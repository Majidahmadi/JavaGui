import javafx.application.Application;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.layout.StackPane;
import javafx.stage.Stage;

public class Main extends Application {
    private Stage stage;

    public static void main(String[] args) {
        launch(args);
        System.out.println("Hello World!");
    }

    @Override
    public void start(Stage primaryStage) {
        stage = primaryStage;
        stage.setTitle("Majid ahmadi");

        Button button = new Button("First Button");
        button.setOnAction(event -> stage.close());
        button.setOnMouseEntered(event -> System.out.println("Enter x= " + event.getX() + " y= " + event.getY()));
        button.setOnMouseMoved(event ->
                //System.out.println("hello");
                System.out.println("Move  x= " + event.getX() + "    y= " + event.getY() + "    z= " + event.getZ())
        );

        StackPane stackPane = new StackPane();
        stackPane.getChildren().addAll(button);
        Scene scene = new Scene(stackPane, 600, 400);
        stage.setScene(scene);
        stage.show();

    }
}
